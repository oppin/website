#### I'm Christopher Johnson and I'm a *Full Stack* PHP Developer living in Copenhagen, Denmark.

I've been coding and building websites since I was at school, back when [Yahoo! GeoCities](https://en.wikipedia.org/wiki/Yahoo!_GeoCities) was still a thing and mobile internet meant [WAP](https://en.wikipedia.org/wiki/Wireless_Application_Protocol). In 2006, I began working freelance on side projects in PHP, building interactive applications from the ground up. Since then I've learnt a lot, and worked on WordPress, Laravel, Symfony, Codeigniter and OctoberCMS projects too.

I love to work with both front-end and backend technologies, because I believe a great design and user experience is important to get the best from any system. With that in mind I've worked lots with SASS, LESS and pure CSS, alongside Vue, React, Vanilla JS and jQuery to create beautiful, powerful and accessible user interfaces.

For the last few years I've begun to delve into server setup and configuration too, helping companies switch from expensive dedicated server setups to lightweight virtual servers. This has brought them a massive reduction in costs and huge improvements in speed and security. I've written continuous integration testing and deployment scripts to staging and production servers, migrating companies from haphazard FTP updates to streamlined automated systems.

Want to know more? [Just drop me a line]({contact})
