### Terms and Conditions
Welcome to our website. If you continue to browse and use this website, you are agreeing to comply with and be bound by the following terms and conditions of use, which together with the privacy policy below govern Oppin Limited's relationship with you in relation to this website. If you disagree with any part of these terms and conditions, please do not use our website.

The terms "I", "our", "us", "we" or "Oppin" refer to Oppin Limited, the owner of this website whose registered office is 27 The Spinney, Bedford, MK41 0HA, UK. Our company registration number is 12101809, registered in England and Wales. The term "you" refers to the user or viewer of our website.

The use of this website is subject to the following terms of use:

The content of the pages of this website is for your general information and use only. It is subject to change without notice.

Neither we nor any third parties provide any warranty or guarantee as to the accuracy, timeliness, performance, completeness or suitability of the information and materials found or offered on this website for any particular purpose. You acknowledge that such information and materials may contain inaccuracies or errors and we expressly exclude liability for any such inaccuracies or errors to the fullest extent permitted by law.

Your use of any information or materials on this website is entirely at your own risk, for which we shall not be liable. It shall be your own responsibility to ensure that any products, services or information available through this website meet your specific requirements.

This website contains material which is owned by or licensed to us. This material includes, but is not limited to, the design, layout, look, appearance and graphics. Reproduction is prohibited other than in accordance with the copyright notice, which forms part of these terms and conditions.

All trade marks reproduced in this website which are not the property of, or licensed to, the operator are acknowledged as the property of their respective owners.

Unauthorised use of this website may give rise to a claim for damages and/or be a criminal offence.

From time to time this website may also include links to other websites. These links are provided for your convenience to provide further information. They do not signify that we endorse the website(s). We have no responsibility for the content of the linked website(s).

Your use of this website and any dispute arising out of such use of the website is subject to the laws of England, Northern Ireland, Scotland and Wales.

***

### Privacy Policy Notice
**The policy:** This privacy policy notice is for this website; oppin.co.uk and served by Oppin Limited and governs the privacy of those who use it. The purpose of this policy is to explain to you how we control, process, handle and protect your personal information while browsing or using this website, including your rights under current laws and regulations. If you do not agree to the following policy you may wish to cease viewing / using this website.

Policy key definitions:

"I", "our", "us", "we" or "Oppin" refer to the limited company, Oppin Limited, registered in England and Wales, number 12101809.    
"you", "the user" refer to the person(s) using this website.    
GDPR means General Data Protection Act.    
PECR means Privacy & Electronic Communications Regulation.    
ICO means Information Commissioner's Office.    
Cookies mean small files stored on a users computer or device.

#### Processing of your personal data
Under the GDPR (General Data Protection Regulation) we control and / or process any personal information about you electronically using the following lawful bases.

<!--{% We are registered with the ICO under the Data Protection Register, our registration number is: ########. %}
{% We are exempt from registration in the ICO Data Protection Register because [provide reason]. %}-->

***

**Lawful basis:** Consent    
**The reason we use this basis:** To respond to initial contact    
**We process your information in the following ways:** To respond to your contact only    
**Data retention period:** We will continue to process your information under this basis until you withdraw consent or it is determined your consent no longer exists.    
**Sharing your information:** We do not share your information with third parties.

***

**Lawful basis:** Contract    
**The reason we use this basis:** To continue providing contracted services    
**We process your information in the following ways:** To communicate with you in relation to our contract and financial agreements    
**Data retention period:** We shall continue to process your information until the contract between us ends or is terminated under any contract terms.    
**Sharing your information:** We may share your personal information with third parties for the purposes of invoicing and financial settlement only.

***

**Lawful basis:** Legal obligation
**The reason we use this basis:** Some services we can offer (such as email) may require us to retain records under a legal requirement. If this is the case, we will inform you at the outset of our contract or as and when any new such requirement comes into force.    
**We process your information in the following ways:** Storing the minimum required data to comply with our legal obligations.    
**Data retention period:** As required by law. We will inform you of this at the outset of our contract or as and when any new such requirement comes into force.    
**Sharing your information:** We do not share your information with third parties.

***

If, as determined by us, the lawful basis upon which we process your personal information changes, we will notify you about the change and any new lawful basis to be used if required. We shall stop processing your personal information if the lawful basis used is no longer relevant.

#### Your individual rights
Under the GDPR your rights are as follows. You can read more about [your rights in details here](https://ico.org.uk/for-organisations/guide-to-the-general-data-protection-regulation-gdpr/individual-rights/).

- the right to be informed;
- the right of access;
- the right to rectification;
- the right to erasure;
- the right to restrict processing;
- the right to data portability;
- the right to object; and
- the right not to be subject to automated decision-making including profiling.

You also have the right to complain to the ICO [www.ico.org.uk](https://www.ico.org.uk) if you feel there is a problem with the way we are handling your data.

We handle subject access requests in accordance with the GDPR.

#### Internet cookies
We use cookies on this website to provide you with a better user experience. We do this by placing a small text file on your device / computer hard drive to track how you use the website, to record or log whether you have seen particular messages that we display, to keep you logged into the website where applicable, to display relevant adverts or content, referred you to a third party website.

Some cookies are required to enjoy and use the full functionality of this website.

<!--We use a cookie control system which allows you to accept the use of cookies, and control which cookies are saved to your device / computer.-->
Some cookies will be saved for specific time periods, where others may last indefinitely. Your web browser should provide you with the controls to manage and delete cookies from your device, please see your web browser options.

<p class="margin-0">Cookies that we use are:</p>
- Google Analytics Tracking to help us understand how the website is being used
- A login session for users who have permission to update the content of the site

#### Data security and protection
We ensure the security of any personal information we hold by using secure data storage technologies and precise procedures in how we store, access and manage that information. Our methods meet the GDPR compliance requirement.

#### Transparent Privacy Explanations
We have provided some further explanations about user privacy and the way we use this website to help promote a transparent and honest user privacy methodology.
