'use strict';

const autoprefixer = require('gulp-autoprefixer');
const gulp = require('gulp');
const modernizr = require('gulp-modernizr');
const sass = require('gulp-sass');
const sourcemaps = require('gulp-sourcemaps');

gulp.task('modernizr', function () {
  return gulp.src('./assets/js/*.js')
    .pipe(modernizr({
      'options': ['setClasses'],
      'tests': ['touchevents']
    }))
    .pipe(gulp.dest('./assets/build/'));
});

gulp.task('sass', function () {
  return gulp.src('./assets/scss/styles.scss')
    .pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer())
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('./assets/css'));
});

gulp.task('sass:watch', function () {
  gulp.watch('./assets/scss/**/*.scss', ['sass']);
});
